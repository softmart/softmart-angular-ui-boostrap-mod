(function() {

	'use strict';

	module.exports = function(grunt) {

		grunt.initConfig({

			pkg: grunt.file.readJSON('package.json'),
			
			uglify: {
				target1: {
					files: {
						'build/js/angular-ui-boostrap-mod.min.js' : ['src/js/ui-bootstrap-custom-tpls-0.12.0.min.js']
					}
				}
			},

		});

		grunt.loadNpmTasks('grunt-contrib-uglify');

		grunt.registerTask('default', [
			'uglify'
		]);

	};

})();